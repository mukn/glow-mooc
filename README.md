# glow-mooc

[Glow course syllabus](https://docs.google.com/document/d/1RgOoK_QOPCyE6t5YafmJv5Cc3RGARPHz/edit)

## Practice specifications

Practices are located in `moduleN/practices/N`.

e.g. `PRACTICE 0-1` is located in `module0/practices/1`.

Practices are not verified on our end.

## Assignment specifications

### Descriptions

`Assignments` have task descriptions located in their root, under `DESCRIPTION.txt`. This is plain-text.

### Directory layout

Assignments are located in `moduleN/assignments/N`

e.g. `ASSIGNMENT 3-3` is located in `module3/assignments/3`

Formats are indicated in [`assignment-formats.json`](./assignment-formats.json)

See below for schema specifications for various formats: `mcq`, `match`, `running commands`.

### MCQ schema

#### Format

`mcq.json`
```json
[{ "question": String, 
   "source": Option<String>,
   "correct-source": Option<String>,
   "answers": [["true|false", String]] 
   "rationale": Option<String> }, ...]
```

#### Example

```json
[
  { "question": "Why learn Glow DSL, a new language?",
    "answers": [
        ["false", "Glow is optimized for low-level, high performance computing."],
        ["true", "Glow is secure. Each newly implemented function is thoroughly audited."],
        ["true", "Glow is portable to any blockchains, users do not need to context-switch between different tools."]
    ]
  },
  { "question": " What is the difference between a dApp and a Smart Contract?",
    "answers": [
        ["false", "dApps exist and are stored on the blockchain and Smart Contracts do not."],
        ["true", "Smart Contracts are programs stored on the blockchain, dApps may only have some application logic stored on the blockchain."]
    ]
  }
]
```

#### Usage

For each question,
1. Display the `"question"`.
2. Show second field of `"answers"`, e.g. `answers.map(answer => answer[1])`.
3. Store user selection
4. Lookup correct / wrong
5. Store the result

After entire MCQ complete, return the results.

### Match assignments schema

#### Format

`match.json`
```js
[{ "language": Language, // language
   "sources": [GlowSrcPath, LanguageSrcPath]
   "matches": [{"purpose": String, "glow": [FragmentIdx], Language: [FragmentIdx]}] }, ...]
```

#### Example

`./lang.txt`
```markdown
Some text written here as a scenario.
```

`./lang.glow`
```js
#lang glow // 0
```

`./lang.sol`
```solidity
pragma solidity ^0.8.2; // 0
```

```js
[{ "language": "solidity",
   "sources": ["lang.glow", "lang.sol"],
   "scenario": "lang.txt",
   "matches": [{ "purpose": "Language Headers"
               , "glow": [0], "solidity": [0]}] }]
```

#### Usage

For each question, display:
1. Display `"scenario"` text.
1. Display code fragments from `"sources"`.
1. Display `"purpose"`s.
1. Request users to match `"purpose"` to fragments of `Glow` and `"language"`.
1. Verify users' indications.

### Running commands

#### Format

1. Instructions in `README.md`. Each section where output is expected is labelled with `N`.
2. Provide expected output in:

    `run.json`
    ``` json
    [ String ]
    ```
    
    The indexes correspond to the `N`

3. User uploads their inputs.
4. We extract the useful parts from inputs, `diff` with expected output.
5. Return results to user.
