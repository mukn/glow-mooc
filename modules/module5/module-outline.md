## **Module 5** : – More advanced DApps

**TEXT / LECTURE 5-1** : Multiple assets

**ASSIGNMENT 5-1** : Write a transaction DApp with multiple assets exchanged

**TEXT / LECTURE 5-2** : Explicit Timeouts

**ASSIGNMENT 5-2** : Add explicit timeouts to your DApp

**TEXT / LECTURE 5-3** : Cross-chain operations

**ASSIGNMENT 5-3** : Moving a Glow smart contract from one blockchain to another
