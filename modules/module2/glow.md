# Introduction to Glow

## Introduction

Hi there, I'm Alex Plotnick, and this is Module 2
of the first-ever Glow online course. In Modules 0 & 1,
Faré and I gave a brief overview of the course, the goals
of Glow, and how to install it and run some basic commands.

In this Module, we'll cover running simple (but non-trivial)
interactions with Glow, and the basics of Glow programming.
The programs we cover in this Module will be small and simple,
but still illustrate quite a lot of the capabilities and
primary concepts in Glow.

Ok, let's get started!

## Our first DApp

The first DApp that we'll explore is called `buy_sig`. We have
an alternate name for it that we use sometimes, which is "closing",
because that's one way to think about what it represents abstractly:
the closing, or signing, of a sale or other agreement between two
parties, as in "we closed on our new house today". I personally
prefer its original name, `buy_sig`, because it perfectly captures
what the program actually *does*: it allows one participant to purchase
a digital signature from another.

In real life, when you close on a sale, the buyer and seller don't
have to trust each other, but they do usually have to trust some
third party, like an escrow agent or notary. But in a *distributed
application*, we don't need that central trusted third party,
because we have a *distributed consensus* (e.g., a blockchain)
that we believe can be trusted (because of proof of work or proof
of stake).

This diagram shows the protocol for this interaction.

[!Closing a Sale](closing.png)

Here we have the two participants, `Buyer` and `Seller`,
on the left and right. And in the middle we have the `Consensus`,
which right now is always a blockchain, and more specifically
right now is basically always Ethereum, but soon could also
be Cardano and maybe some others. So for now we'll assume
it's Ethereum or some other network that runs the Ethereum
Virtual Machine, and I will talk about "accounts" rather than
UTxOs.

The idea of this interaction is that `Buyer` wants to pay `Seller`
for their signature on some message, which we'll take to be an
arbitrary hash, or digest. By this I mean it's the output of a
cryptographic hash function like SHA-256. The object that's hashed
could be a PDF that describes the terms of the sale in english,
or a blob of XML or JSON that describes it using some schema,
or whatever; but we'll only be concerned with its hash, and ignore
the underlying object.

The interaction proceeds like this: (time goes down in this diagram)
 
First, the Buyer sets up the `buy_sig` contract, including the
digest to be signed, the participants' addresses, and so on; and
they also deposit the agreed-upon Price (in whatever token) into
the contract account; that is the escrow. Now, if there's an error,
or timeout, or the seller doesn't cooperate, it's very important
that the escrow be returned to the Buyer; we'll come back to this
point in a few minutes.
 
Then `Seller`, after checking the agreement and seeing that
everything's all right, signs the digest and publishes that
signature on the chain.

The final step happens entirely on the chain: the signature is
checked against `Seller`'s public key, and if it matches, the
escrow is released to `Seller`. The terms of the contract have
been fulfilled, so the interaction is over.

All right, let's see this interaction run in Glow.

## Running a Glow program

So, to run `buy_sig`, we'll actually need to run two instances
of Glow, one for the buyer, and one for the seller. I'll run
those in the two terminals you see here. If we run
`glow list-identities`, we can see the two identities that I'll
use for this run, which I've called `alice` and `bob`. I've
generated these addresses specially to encode these names so
we can easily distinguish them in the transaction output.

Last bits of setup: I'm running a local Ethereum test network
here with `geth`, which was started with our `run-ethereum-test-net`
script. And I've funded both of these accounts with `glow faucet`.
(You might think you could just fund the buyer, but the seller
also needs to pay gas fees, so needs a small amount of token as well.)

Ok, let's (finally!) run our first Glow DApp. I'll do this long-hand
the first time through, and then I'll show you how we can shortcut
some of the interactive steps and run it in more of a "batch" mode.

We start a Glow interaction with (unsurprisingly) the command
`glow start-interaction --evm-network pet`. So we have to tell
it which network to use, but for now we won't tell it anything else.

So it starts by asking us which DApp we want to run. And the
three choices here are what's shown in the output of
`glow list-applications`; we'll select `buy_sig` as option 1.

Now I have to tell it which identities to use. Here in the top
terminal I'll be `alice`, and in the bottom one I'll be `bob`.
So we select the identity, and then we select which role that
identity will play in the interaction. Let's have `alice` buy
the signature from `bob`. So we select `alice` as the `Buyer`,
and `bob` as the `Seller`.

Now it asks us which token we want to use, and here we'll
stick to the native token, `PET`. (But you can, for example,
write contracts that swap native tokens for ERC20 tokens.)

Then comes two very important parameters, the digest that
we'd like signed, and the price we'd like to pay for it.
I have here a totally random hash (it was actually a transaction
ID, but whatever, it's a hash), and let's say that `bob` wants
my favorite amount, π PET (expressed in "wei", or 10^-18 PET)
for his signature on this extremely important digest.

The last parameter we're prompted for is the "Max initial block",
which is the block number on the selected network that is the
last acceptable block to start the interaction by. We'll come
back and discuss this parameter a little later in the course,
but for now let's just take the current block.

Ok, now Glow sets up the on-chain code, and waits for the
seller to make his move. How long it waits is configurable,
and we'll come back to that later, too, when we talk about
timeouts.

All right, so let's do the seller's side now. We start the
interaction in the same way, but we have to add this additional
argument `--database bob` in order to keep the transaction
databases between the two participants separate. (This limitation
will be removed in the future.)

So here we're `bob`, and playing the role of `Seller`, and our
counter-party, the `Buyer`, is `alice`. All of the other parameters
must match exactly, so we'll just copy and paste from above.

All right, now Glow tells us "Paste below the handshake sent
by the other participant". What this means is that the `Buyer`
and `Seller` are all set on the agreement to purchase this signature,
but they need to tell Glow that by means of this virtual handshake.
And this handshake has to be exchanged *off-chain*, for reasons
we'll come back to when we use a communications channel more
realistic than my clipboard. But for this demonstration, we'll
just copy and paste the handshake from one terminal to the other;
In a real DApp, we'd obviously need to use some sort of network,
and we'll see a little later in the course how to do that.

Now, as soon as I paste this handshake into `bob`'s instance
of Glow, it executes the `Seller`'s one step, which is to sign
the digest and publish it on the chain. And notice that once
*that* happens, the program terminates on both ends: the
signature is valid, so the escrow is transferred to `bob`,
and the interaction is over.

Ok, we've just run our first Glow interaction! Let's look at
the code.

## Reading a Glow program

Now that we've seen the `buy_sig` program run and understand
what it does, let's look at the implementation. It is 6 lines
of Glow, not counting 1 blank line to separate the buyer's
move from the seller's.

Let's take a look at what's going on here.

### JavaScript-like pure functional core

The place to start is actually not line 1, but line 2.
Let's cut away some of this unfamiliar looking stuff,
and see if we can get the basic structure:

```
let buySig = (digest: Digest, price: Nat) => {...};
```

The core of Glow is a JavaScript-like purely functional language.
This is a JavaScript `let` binding: it declares a variable called
`buySig`, whose value is a function, expressed with the JavaScript
"fat arrow" notation: `(digest: Digest, price: Nat)` is the parameter
list of the function, and `{...}` is its body. The `;` terminates
the `let` statement (and is mandatory).

Let's look at the parameter list for a minute. We're defining
a function here that takes two arguments, which we'll call
`digest` and `price`. Now, the capitalized symbols _after_
the colons are types: `Digest` and `Nat` are built-in types
that represent the output of a hash (which may be network
specific), and a natural number (i.e., a non-negative integer,
which for EVM based networks is always exactly 256 bits wide).

All right, so it's a function definition. That's great, but
what does it do with that function, and what does the function
do? So let's restore our line 1, but not the body for now.

```
@interaction([Buyer, Seller])
let buySig = (digest: Digest, price: Nat) => {...};
```

### @-annotations

Line 1 does not look like JavaScript. It's a design principle
of Glow that if it doesn't look like JavaScript, it isn't.
So this is a Glow annotation. The `@` sign means that this
annotation *modifies the meaning* of the JavaScript statement
that follows. The JavaScript statement that follows is the
`let` binding we were just talking about. And its new meaning is
that this function, the one bound by `let`, defines an interaction
with roles `[Buyer, Seller]` (that's a two-element list).
(In principle, Glow supports more than two roles per interaction
and eventually arbitrary numbers, but we're going to stick with
two for now.)

I'll say one more thing about `@` annotations in Glow.
Well, two things. One is that the complete list of annotations
and other special forms can be found in the
[Glow Reference Manual](https://glow-lang.org/docs/Glow_Reference_Manual.html).

The second is about the semantics of these annotations generally.
These are DApp-specific annotations. The idea is that if you were
to *erase* all of the `@` annotations from your Glow program,
*and* if all of the roles trusted each other,
*and* if the program was running on a centralized computer,
*then* the results would be the same as running distributed
and *with* the annotations. So these annotations control *where*
and *how* a piece of code in the DApp runs, but if you were to
"collapse" the program into one place and assume trust and good
intentions (dubious assumptions, to be sure), then those
distinctions wouldn't matter. So these annotations allow you
to write code that allows the app to be distributed and for
the roles to not necessarily trust one another.

### !-directives

Ok, let's dive into the function body now.

On line 3, we see this `deposit!` thing, and it doesn't
look like JavaScript. So it isn't. It's a Glow directive
that has a side-effect on the chain: that's what the `!`
indicates. (This is a convention we got from Scheme.)
And the syntax of this directive is as follows: there's
a role, a "right arrow", and an expression (here a variable,
the argument `price`). And what it *means* is that the
identity that assumes that role at run-time should deposit
the result of the expression into the contract account
to be held in escrow.

That's the first step of this contract: the buyer deposits
the escrow. And in fact it's the buyer's only move.

### Seller's side

Ok, now we come to the seller's side. Line 5 starts with
an `@` annotation; let's erase it for now to see what's
happening first:

```
let signature = sign(digest);
```

Straightforward enough: it's a JavaScript `let` binding,
the variable bound is called `signature`, and the value
it's bound to is the result of calling the `sign` function
with argument `digest` (one of this function's arguments).

Ok, so `sign` is another Glow built-in, but it's not really
DApp-specific or anything, so it doesn't have a special syntax;
you can just call it like a normal function. And what it does
is produce a cryptographic signature of the argument you pass in.
It does this in a way that may be network-specific: the EVM
uses ECDSA over the elliptic curve secp256k1, but other networks
may use other signature algorithms, or other curves, or RSA,
or whatever. This is a simple but convenient abstraction that
Glow gives you: you don't have to worry about or even remember
the specifics of such things, you just `sign` the `digest`.

Now let's put the annotation back: `@publicly!(Seller)`.
This one's a little funny: it's both an `@` annotation and
a `!` directive! And that's because it does two things:
it modifies the meaning of the `let` statement that follows,
*and* it has a side-effect on the chain. What this annotation
does is to write the value of the variable bound here onto the
chain, which must be a digital signature, *and* that signature
must be valid.

Just to make this really clear, here's what this annotation
expands to inside the Glow compiler.

```
  @publicly!(Seller) let signature = sign(digest);
  // The line above is equivalent to the three below:
  //// @verifiably!(Seller) let signature = sign(digest);
  //// publish! Seller -> signature;
  //// verify! signature; // This line is itself the same as the one below:
  ////// require! isValidSignature(Seller, signature, digest);
```

So it's really two statements in one: it publishes a signature
on the chain, and it ensures that everyone can verify that it
is a valid signature for the seller.

Ok, only one line left. Line 6 is the converse of line 3:
it withdraws the escrow into the seller's account. Now if,
for instance, the public signature verification on line 5
had failed, this statement would never be executed: what
would happen then?

The answer is that Glow will refund the escrow to the buyer,
because it tracks all of the `deposit!/withdraw!` statements
and ensures that they are correctly balanced or refunded one
way or another. The idea here is that it's *really hard* to
accidentally lock your tokens up in a Glow contract, unlike
some lower-level languages, where it's really easy.

Let's go through the whole thing once more, but faster this time.
This is a Glow program that describes an interaction between two
roles, `Buyer` and `Seller`. It takes two parameters, the digest
for `Seller` to sign, and the `price` they've agreed on. And
remember that Glow told us to `Define parameters`, and then asked
for values of `digest` and `price` - the two parameters here.

The function itself does three things (or really two-and-a-half):
`Buyer` deposits `price` in escrow, `Seller` signs `digest` publicly
(i.e., verifiably), and then withdraws the escrowed `price`.

And that's it! That's the whole signature buying protocol we talked
about at the beginning. I like this example because there's not a
single line you can really argue about: each statement's purpose
follows directly from the protocol.

## What's going on under the hood?

To compile this Glow program, the `glow` compiler uses a process
called end-point projection to produce three pieces of code: one
for the chain, and one for each participant in the interaction.

Those participants have identities, which are mapped to the roles
in the interaction. That's what the first few questions were about
that `glow` asked us when we ran it; building that participant/role map.

Once that map is built (or it can be supplied up front), each
`glow` instance assumes a particular identity, and then interprets
that participant's code, which can (and does here) differ from
other participants' code. And the code that runs on the chain is
naturally specific to the chain: when `glow` runs on an EVM-compatible
network, it compiles the consensus code directly to EVM bytecode,
which is executed "by the chain", i.e., by every verification node.

If you're curious what the emitted code looks like, you can find
some examples in our GitHub repo, under `t/passdata`, or you can
run, e.g., `glow pass project buy_sig.glow`. We will not be getting
into the details of that in this course, but if you're interested
in hacking on `glow` itself or just understanding what it's doing
at a deeper level, I'd recommend checking it out.

## Our second DApp: Rock, Paper, Scissors

Let's now take a look at a slightly less trivial DApp. Well,
actually its purpose is more trivial, but the program is less.
This is a Glow program that lets two players, here denoted by
the roles `A` and `B`, to play a game of rock-paper-scissors.

In real life, to play this game, the two players "shoot" a
hand shaped like either "rock", "paper", or "scissors", and
the rules are that paper beats rock, scissors beats paper,
and rock beats scissors. If the two hands are the same, it's
a draw.

So, to play this game on a blockchain, we need to somehow encode
the rules and the playing. And here's the basic encoding: we'll
use natural numbers between 0 and 2 inclusive to represent the
hands: 0 = Rock, 1 = Paper, 2 = Scissors.

And to figure out who won from two such hands, we can use a tiny
bit of modular arithmetic: that's what the function `winner` here
at the top does:

```
let winner = (handA: Nat, handB: Nat) : Nat => {
    (handA + (4 - handB)) % 3
};
```

It takes the encoded hands as arguments, and returns a code
that denotes who won: if the result is 0, `B` wins, if 1,
it's a draw, and if `2` then `A` wins.

Now let's look at this next function. Here we see the same
kind of `@interaction` annotation we saw on `buySig`, and
as before there are two roles. This function takes just one
argument, the `wagerAmount`, which is analogous to the `price`
in `buySig`; it's the amount that `A` and `B` have agreed to
bet on the outcome of the game. (We'll assume they're betting
the same amount, and not playing with odds or anything.)

We immediately see something new in the body of this
function: `@A`. What does that mean? Well, it's an `@`
annotation, so let's see if we can understand the statement
it modifies. That's a `let` binding for `handA`, and its
value is the result of calling the `input` function with
these two arguments. `input` is again a Glow built-in,
and what it does is to prompt the user for a value of the
given type, here `Nat`, using the prompt string given as
the second argument. So basically this asks the user for
a number.

What the `@A` does is modify the meaning of the binding so
that it's a *private* binding: only the participant playing
the role `A` will run that statement, and so only `A` will
see what hand is chosen. That's crucial because if `B` could
see `A`'s hand ahead of time, they could just choose a winning
hand every time.

The next two lines we see are also run privately by `A`.
The `require!` just checks that the value is valid according
to our encoding: it has to be strictly less than 3 (and >= 0,
because it's a `Nat`).

So, to understand the next few lines, we have to go back
and talk about how the game play itself works. We can't ask
two participants to do something "simultaneously" on a blockchain.
But what we can do is have one player "commit" to a hand and
publish that commitment publicly, then have the other player
reveal their hand, and finally have the first player reveal
theirs, and have everyone check that the revelation matches
the commitment.

And so that's what these next two lines do: we'll use a
cryptographic digest as `A`'s commitment to their hand.

Now I need to talk about digests and "salts" for a second.
So, a hash function used as a digest is a one-way transformation
of some input into a random string of bits. And with good, modern
hash functions, no one knows how to beat that "one-way" part.
But they can *precompute* hashes of possible inputs, and compare
the hash values. And in this case, because `handA` stores a natural
number less than 2, there are only three possible inputs (i.e.,
"rock", "paper", or "scissors"). So if we were to just hash the
hand, we couldn't tell *just* from the hash which hand it was,
but if our opponent hashed all three possible hands and then
compared the hashes, they *could* tell which hand it was.

So a "salt" is a bit of randomness we throw in to ensure that
they can't precompute the hashes, because they can't know what
salt will be chosen. And of course it's essential that the salt
be truly random, and not under adversarial control, etc. (We'll
get more into these kinds of threat models later in the course.)
So what's happening here when we call `digest(salt, handA)` is
that we're using this random salt to "mix up" the hash of `handA`.

So this line calls another Glow built-in function, `randomUInt256`.
That function does what it says on the tin: it gives you a fresh,
random, 256-bit unsigned integer. And that integer is known only
to `A` at this time, because of the `@A` annotation, and that's
our salt.

Now, the value of that digest is bound to a variable called
`commitment`, and modified by a `@verifiably!` annotation.
And then `A` publishes that commitment on the chain, and
deposits their wager into escrow.

Now let's see what happens with `B`. The first thing that happens
is they privately choose a hand; that's what the `@B` annotation
on `handB` does. Then they immediately publish it directly
(i.e., without hashing), and deposit their wager. Why?
Because `A` has already committed to their hand, so they
can't choose a new one after seeing `B`'s hand. And the reason
the `let` has to be private to `B` is just that we don't want
anyone *else* to choose the hand, in particular `A`. So this
`input` is executed only by `B`, who then publishes their hand,
allows everyone to verify it, and puts their wager in escrow.

Now let's look at these next three lines.

```
publish! A -> salt, handA;
require! (handA < 3);
verify! commitment;
```

The first says that `A` now publishes their two "secret" pieces
of data: the `salt`, and `handA`. Having done so, *everyone* can
validate that the hand is valid, and furthermore can `verify!`
the commitment. This is analogous to the verification of the
cryptographic signature we saw in `buySig`, except here it's
verifying the cryptographic digest. And again, all the participants
(including the chain) can now do this verification, since the
necessary inputs have all been published.

Ok, first homework: stare and play with these `publish!`,
`deposit!`, `require!`, and `verify!` statements and figure
out which ones can be reordered and which can't. For instance,
I'll give you one for free: the line `require! (handB < 3);`
could precede `deposit! B -> wagerAmount;`, but not before
`publish! B -> handB;`. Try to figure out the dependencies
of all the other statements in this part of the program.

Ok, almost there. What's happened so far is this: `A` chose
their hand, published a verifiable commitment to that hand,
and deposited their wager. Then `B` chooses a hand, and
immediately publishes it and deposits their wager. Then `A`
revealed their hand, and everyone verifies that it matches
the published commitment.

So all that's left is to figure out who won, and distribute
the winnings. And that's what happens here:

```
    let outcome = winner(handA, handB);
    switch (outcome) {
      | 0 => withdraw! B <- 2*wagerAmount // B wins
      | 1 => withdraw! A <- wagerAmount; withdraw! B <- wagerAmount // A wins
      | 2 => withdraw! A <- 2*wagerAmount // Draw
    };
```

We call the `winner` function defined up at the top, and `switch`
on the encoded result. If `B` wins, we give them twice the wager
amount (their bet plus `A`'s); if it's a draw, they both get their
wagers returned, and if `A` wins they get twice their wager back.
And finally the function returns the outcome, but we won't use
that return value right now.

### Running the DApp

Ok, now that we've gone so carefully over the code, let's actually
see it run.

So we'll start in the same way as `buy_sig`, with
`glow start-interaction --evm-network pet`. But now
we'll select `rps_simple` as our DApp, and we'll have
`alice` play the `A` role, and `bob` play the `B` role.
And we'll bet π PET again, and use the current block as
the max initial block.

And now we hit the first `input`, the one that runs privately
for `A` to get their hand. So let's do the boring thing, and
pick "rock", or 0.

Ok, so now `A` is ready to go, and waits for `B` to make
their move. So let's fire up `B`, and assign our roles,
and the wager, and the max initial block, and here we go.

And now we hit that second `input`, this time running privately
for `B`. And let's be very very boring here and also enter
rock, just to see what happens. So now `B` says waiting for `A`,
and that's because `A` needs to reveal their salt and hand.
And once that happens, both sides are done! The outcome is 1,
which if you remember was `winner`'s encoding for a draw, which
of course two identical hands should produce.

### Homework

Now I'd like to assign you some homework. There are four parts,
and you can do as many as you feel comfortable with or have time
for.

The first part should be really easy if you've followed along:
run `buy_sig` and `rps_simple` for yourself.

The next parts require you to write your own DApp, which is really
the subject of the next lecture, but I can show you the basics
very quickly. There's an environment variable called `GLOW_PATH`
that defines which directories `glow` looks in for its DApps.
So if we set it to, say, `/tmp/dapp` and then copy `buy_sig`
to some file in that directory, let's say, `sell_sig.glow`,
then it'll find it under that name with `glow list-applications`
and `glow start-interaction`.

Ok, so the three non-trivial parts of the homework are:

1. Write an interaction that transfers some amount of token
   between two participants.
2. Modify `buy_sig` to do something else. It could be some
   other form of sale, or a simple swap, or maybe a discount,
   it doesn't matter. The point is to make something up and
   encode it using Glow, using a variation of this simple contract.
3. Modify `rps_simple` to play a new game. This could be evens
   and odds, or any other simple two-player game where they each
   choose some hand and some simple decision procedure determines
   the winner. Use what you've learned about RPS to design an
   encoding for both the hands and the game play, and modify the
   RPS contract for your game. Then make sure you can play it,
   and that it's fun!

## Appendix: Running Glow in "batch mode", UI sneak peek

Well, we've covered a lot of ground here, and I'm not going to
keep you much longer. We'll wrap up with a quick note on running
`glow` in "batch mode", and a "sneak peek" at the UI, which makes
running these kind of standard interactions a lot easier.

### Getting a digest

We're going to circle back to `buy_sig` for this example.
Now, one question you'll immediately have when you start
playing with this is "but where do I get the digest to be
signed"? And of course there are many ways, but one easy way
is with `glow digest`, e.g., `glow digest /etc/profile`,
or whatever file you'd like a hash of. And if you'd like to
practice with hash functions, this is a great way: just echo
(without a trailing newline) a string into a file, and then
ask for its digest:
`echo -n 'foo bar' > /tmp/foo && glow digest /tmp/foo`

Now, if you've been paying careful attention, you'll notice
that there's a potential bug in this command, which is that
it doesn't take a network option. It should, though, because
different networks use different native hash functions. But
it's fine for now, because as I said before, Glow mostly only
supports Ethereum-based networks currently, which all use the
Keccak256 hash function.

### Batch mode

Ok, so that's one way to get a digest value for `buy_sig`.
I promised earlier that I'd show you how to run that interaction
in a less interactive way. And the reason that's nice is that
if you're hacking on it, or even just playing around with running
it in a bunch of different ways, you don't want to have to go
through the whole interactive dialog, twice (once for the buyer,
and again for the seller).

So the way we give all the parameters at once looks like this:

```
glow start-interaction \
--evm-network pet \
--max-initial-block '%10000' \
--glow-app buy_sig \
--my-identity alice \
--role Buyer \
--database alice \
--assets '{"DefaultToken": "PET"}' \
--participants '{"Buyer": "0xa11cE3d7466d87169693843785b4bAa89B1BeA94", "Seller": "0xb0bCd0F25DAa620352FDeD9824527081265447eF"}' \
--params '{"digest": "0x87ae0eb13d7cb064c81c9722b7a1752b8720e148b9fdbfbaff0af50bb83e5afd", "price": 314159265358979323}'
```

Let's go through each option here. We already know `--evm-network pet`.
`--max-initial-block` looks kind of funny, though. What's going on there
is a little bit of a trick: we need the two `glow` instances to agree on
this parameter, but we don't want to depend on its current value, which
we don't know ahead of time, and can't guarantee will be the same for
the two participants. So what `%10000` means is "the next multiple of
10000 from the current block", which should be the same for both of them
if they start even remotely close to each other. You can experiment
with this parameter and see how low you can get it, if you'd like.
Ok, the next option `--glow-app` tells Glow what DApp to run.
The next specifies the identity to use, and here we can use a nickname.
Then comes the role we'll play as that identity, and then that `--database`
flag that'll just keep the transaction logs separate.

Now the last three are a little different, and you'll notice that they look
a lot like JSON. And so they are. These are blobs of JSON for the asset,
participant, and parameter maps, respectively. These are not really
designed to be entered manually, but are rather for programs or user
interfaces to generate programmatically. So that's why it's a little ugly,
but it's better than typing or pasting the same thing a zillion times while
you're debugging your contract.

Let's see it run. So here on the buyer's side, there's no interactive
prompts for input, it goes right to setting up the contract and waiting
for the seller. On the seller's side, we have to adjust the options
appropriately, making sure we're `bob`, playing the `Seller` role, etc.
And then this side goes right to the handshake exchange. Now, as we
talked about briefly earlier, and will return to again later in the
course, there are other ways of handling this exchange, but for now
we'll keep it as the one manual step. So I'll just copy & paste it
between the terminals as before. And then both sides complete successfully,
because they have all the information they need.

### UI demo

All right, let's finally see this UI. We'll go into this more
later on, but I wanted to give you a preview of "the easy way"
now that you've struggled through "the hard way". So, here we are.

This is the Glow UI. It's based around the contacts: here we
see our three contacts "Alex", "Alice", and "Bob". So let's
run `buy_sig` one more time between Alice and Bob. So here in
this browser tab I'll be Alice, and in that one I'll be Bob.
So Alice picks an identity, and then chooses an action; here
we'll select "Buy signature from...", and choose to buy it
from Bob. Then we have to enter our parameters, the `digest`
and `price`. And then it starts a glow interaction, in almost
exactly the batch style I just showed you, and it's waiting
for the other side to start.

So we start Bob running the other end by select "Sell signature to...",
and choose Alice. Enter the parameters, start it up, and voila!
Both sides complete the transaction successfully.

### Final thoughts

Ok, that about wraps it up for this time. We've seen how to run
DApps interactively, we've examined the basic syntax and semantics
of the Glow programs that implement those DApps, we've talked about
encodings and protocols, and seen a few different ways of running
them. We're sure you're going to have many questions, and we are
looking forward to our first live Q & A, on October 12, 2021 at
10 PM CET, 4 PM EDT, 2PM MDT on Discord.

We would like to thank you once again for being part of this very
first iteration of the Glow MOOC! We'll see you soon!
