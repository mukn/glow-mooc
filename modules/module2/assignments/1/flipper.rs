// 1
use ink_lang as ink;

// 2
#[ink::contract]
mod flipper {
    #[ink(storage)]
    pub struct Flipper {
        value: bool,
    }

// 3
    impl Flipper {
        #[ink(constructor)]
        pub fn new(init_value: bool) -> Self {
            Self {
                value: init_value,
            }
        }

// 4
        #[ink(message)]
        pub fn flip(&mut self) {
            self.value = !self.value;
        }

// 5
        #[ink(message)]
        pub fn get(&self) -> bool {
            self.value
        }
    }
}
