-- 1
import           Control.Monad             (void)
import qualified Data.ByteString.Char8     as C
import           Language.Plutus.Contract
import qualified Language.PlutusTx         as PlutusTx
import           Language.PlutusTx.Prelude hiding (pure, (<$>))
import           Ledger                    (Address, Validator, ValidatorCtx, Value, scriptAddress)
import qualified Ledger.Constraints        as Constraints
import qualified Ledger.Typed.Scripts      as Scripts
import           Playground.Contract
import qualified Prelude

-- 2
newtype HashedNum = HashedNum ByteString deriving newtype PlutusTx.IsData

PlutusTx.makeLift ''HashedNum

newtype ClearNum = ClearNum ByteString deriving newtype PlutusTx.IsData

PlutusTx.makeLift ''ClearNum

hashNum :: Int -> HashedNum
hashNum = HashedNum . sha2_256 . C.pack . show

clearNum :: Int -> ClearNum
clearNum = ClearNum . C.pack . show

-- 3
type GameSchema =
    BlockchainActions
        .\/ Endpoint "lock" LockParams
        .\/ Endpoint "guess" GuessParams

data Game
instance Scripts.ScriptType Game where
    type instance RedeemerType Game = ClearNum
    type instance DatumType Game = HashedNum

gameInstance :: Scripts.ScriptInstance Game
gameInstance = Scripts.validator @Game
    $$(PlutusTx.compile [|| validateGuess ||])
    $$(PlutusTx.compile [|| wrap ||]) where
        wrap = Scripts.wrapValidator @HashedNum @ClearNum

-- 4
validateGuess :: HashedNum -> ClearNum -> ValidatorCtx -> Bool
validateGuess (HashedNum actual) (ClearNum guess') _ = actual == sha2_256 guess'

gameValidator :: Validator
gameValidator = Scripts.validatorScript gameInstance

gameAddress :: Address
gameAddress = Ledger.scriptAddress gameValidator

-- 5
data LockParams = LockParams
    { secretNumber :: Int
    , amount     :: Value
    }
    deriving stock (Prelude.Eq, Prelude.Show, Generic)
    deriving anyclass (FromJSON, ToJSON, IotsType, ToSchema, ToArgument)

-- 6
newtype GuessParams = GuessParams
    { guessNumber :: Int
    }
    deriving stock (Prelude.Eq, Prelude.Show, Generic)
    deriving anyclass (FromJSON, ToJSON, IotsType, ToSchema, ToArgument)

-- 7
lock :: AsContractError e => Contract GameSchema e ()
lock = do
    LockParams secret amt <- endpoint @"lock" @LockParams
    let tx         = Constraints.mustPayToTheScript (hashNum secret) amt
    void (submitTxConstraints gameInstance tx)

-- 8
guess :: AsContractError e => Contract GameSchema e ()
guess = do
    GuessParams theGuess <- endpoint @"guess" @GuessParams
    unspentOutputs <- utxoAt gameAddress
    let redeemer = clearNum theGuess
        tx       = collectFromScript unspentOutputs redeemer
    void (submitTxConstraintsSpending gameInstance unspentOutputs tx)

-- 9
game :: AsContractError e => Contract GameSchema e ()
game = lock `select` guess

endpoints :: AsContractError e => Contract GameSchema e ()
endpoints = game

-- 10
mkSchemaDefinitions ''GameSchema

$(mkKnownCurrencies [])
