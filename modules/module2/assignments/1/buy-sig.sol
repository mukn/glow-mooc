// 1
pragma solidity ^0.8.2;

// 2
contract ClosingBug {
  address payable Buyer;
  address payable Seller;
  bytes32 digest;
  uint price;

  constructor(address payable _Buyer, address payable _Seller,
              bytes32 _digest, uint _price) payable {
    Buyer = _Buyer;
    Seller = _Seller;
    digest = _digest;
    price = _price;

// 3
    require(msg.value == price);
  }

// 4
  event SignaturePublished(uint8 v, bytes32 r, bytes32 s);

  function sign(uint8 v, bytes32 r, bytes32 s) public payable {
    require(Seller == ecrecover(digest, v, r, s)); // Verifies the digest was signed
    emit SignaturePublished(v, r, s); // Indicate Seller's signature has been published

// 5
    selfdestruct(payable(Seller)); // Payment should be transferred
  }
}
