# ASSIGNMENT 2-1

This assignment will bring you through contracts written in `Glow`,
and their counterparts in `Solidity` and `Plutus`.

Through this you will understand differences between these smart contract languages,
and the benefits `Glow` brings about due to its expressiveness.

# Contract 1: Buying a Signature

`Solidity` Contract adapted from [*François-René Rideau's (@fare)* talk](https://glow-lang.org/talks/security-through-clarity-2021/)

## Scenario

Let's consider the simplest possible Decentralized Application (DApp): the closing of a sale. The intent of the DApp is as follows: a Buyer and a Seller have agreed to the terms of a sale. The Seller will sign a title transfer, and the Buyer will pay a price for it in tokens. The title transfer may be an electronic copy of a legal document, a transaction on another blockchain, a keycode for a hotel room, or anything in between, etc.

Because they want the transaction to be trustless, the two participants use a blockchain smart contract to ensure that their interaction will be “atomic”, i.e. all or nothing: either they both cooperate and the transaction happens, or one fails to cooperate and then nothing bad happens to the other participant except wasting a bit of time and a small amount of transaction fees.

## Task

Your task is to match fragments of `Solidity` code to their `Glow` counterparts.

Note: Instead of initializing the contract with the title transfer as an input, the contract takes a *digest*, which is a hash of the original document (sending over the entire document is expensive).

## Solution

| Glow | Solidity | Remarks                                              |
|------|----------|------------------------------------------------------|
| 1    | 1        | Boilerplate                                          |
| 2    | 2        | Initialization / Specify Interface                   |
| 3    | 3        | Depositing the amount payable into escrow from Buyer |
| 4    | 4        | Sign and publish this event                          |
| 5    | 5        | Withdraw amount from escrow to Seller                |

# Contract 2: Flipping a boolean value

`Ink! - Flipper` Contract taken from [Flipper](https://github.com/paritytech/ink#examples).

## Scenario

We want to flip a *boolean* value and see the result.

## Task

Your task is to match fragments of `Ink!` to their `Glow` counterparts.

## Solution

| Glow | Ink! | Remarks                                                          |
|------|------|------------------------------------------------------------------|
| 1    | 1    | Boilerplate                                                      |
| 2    | 2,3  | Initializing the contract with values                            |
| 3    | 4,5  | Flipping the boolean value and getting / returning the new value |

# Contract 3: Guessing Game

`Plutus` Contract adapted from [Game Contract](https://github.com/input-output-hk/plutus-starter/blob/main/examples/src/Plutus/Contracts/Game.hs).

## Scenario

We play a Guessing game where Game Master supplies a word, and a reward for guessing it. The player will attempt to guess the word, and withdraw the reward if they succeed. Otherwise the reward goes back to the Game Master.

## Task

Your task is to match fragments of `Plutus` code to their `Glow` counterparts.

## Solution

| Glow | Plutus | Remarks                                                |
|------|--------|--------------------------------------------------------|
| 1    | 1      | Boilerplate                                            |
| -    | 2      | Compiler takes care of datatype serialization for Glow |
| 2    | 3      | Specification of interface                             |
| 5    | 4      | Validation logic                                       |
| -    | 5      | Only Primitive datatypes used in Glow currently        |
| -    | 6      | Only Primitive datatypes used in Glow currently        |
| 3    | 7      | Ensure payment is made, supply, hash word              |
| 4    | 8      | Player makes the guess                                 |
| 2    | 9      | Specification of interface                             |
| 1    | 10     | Boilerplate                                            |

# Notes for those using these resources as graded assignments

You should scramble the fragments in `*fragmented*` files.
