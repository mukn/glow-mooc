# Implementing a simple transaction contract in Glow

The goal of this practice is to implement a smart contract, where one party can transfer funds to another.

The task is in `simple-funds-transfer.glow`. Fill in the `...` sections.

The solution is in `simple-funds-transfer.solution.glow`.
