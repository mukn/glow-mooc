## **Module 2** : First steps

**VIDEO 2-1 and TEXT 2-1** : Explanation on how to see multiple users, and what the common syntaxic artefacts are in Glow.

**PRACTICE 2-1** : Writing the most basic interaction between two parties in Glow : a simplement money transfer

**LECTURE 2-1** : Analysis of a simple money transfer in several different languages (solidity, scylla, vyper, OcamL, ink!, Michelson, etc…). Each time, the student will be able to check two things :
1.   	That the number of lines of code is significantly lower in Glow
2.   	That there are opportunities for hard-to-spot bugs
 
A**SSIGNMENT 2-1 (optional ?)**: Make different fragments of code correspond with each other, by copy-pasting it. The student must select « line X to line Y » in Glow and make it correspond to « line X’ to line Y’ » in another language.

**TEXT 2-2** : The existing smart-contracts in the Glow library

**PRACTICE 2-2** : Execute an existing smart-contract with suitable parameters in Glow

**ASSIGNEMENT 2-2** : Spot the wrong parameters in the execution of an executed smart contract
