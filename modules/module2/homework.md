# Module 2 Homework

You may do as many parts as you would like, but parts 1 & 2
are highly recommended. They constitute "dipping your toes in";
if you want to plunge deeper in, try parts 3 & 4.

## Part 1

Run `buy_sig`, playing both `Buyer` and `Seller` roles.

## Part 2

Write a Glow program that transfers some amount of (native)
token between two participants.

Hint: the body of your `transfer` function should be 2 lines.

## Part 3

Modify `buy_sig.glow` to do "something else". This is intentionally
vague: use your imagination to come up with some other very simple
kind of sale or swap that requires just a little more protocol than
`transfer`. Make sure you can run it between two identities you
control.

Hint: you're going to want a `glow balance` command, which we don't
currently have (but will be added soon). In the meantime, you can use
`glow faucet`, which shows the ending balance after funding.

## Part 4

Modify `rps_simple` to play a new game. Suggestions for possible
games: evens & odds, guess a number between 1 and 10, etc. Use
what you've learned about RPS to design an encoding for the hands
and a protocol for playing. Make sure you can run it and the
outcomes are what you expect.
