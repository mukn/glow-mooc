## **Module 8** – Off-chain communication

**TEXT 8-1** : Basic concepts in Javascript

**PRACTICE 8-1** : Negotiating contracts off-chain with the Glow UI

**PRACTICE 8-2** : Integrating with libp2p
