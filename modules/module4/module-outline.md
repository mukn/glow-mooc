## **Module 4** : ADVANCED LOGIC

**TEXT / LECTURE 4-1** : how to reason about DApps ?  --- CTL, Linear Logic, Epistemic Logic, Game Theory, Game Semantics, etc.

**ASSIGNMENT 4-1** : Spot different Game Theory patterns in a blockchain ecosystem

**ASSIGNMENT 4-2** : Are those Glow smart contracts and DApps correct or not ? Spot the game theory issue
