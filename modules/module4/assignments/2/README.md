# ASSIGNMENT 4-2

This assignment will apply Game Theory in the Context of Glow DApps.
You will understand the different kinds of exploits that can happen as a result of information asymmetry.

Your task is to look at given *Problems* and identify the issue with them.

## Contract 1

### Scenario

This is a standard closing contract,
wherein a Buyer pays money in exchange for a Seller signing a transaction:
digital title transfer, transaction that completes an atomic swap on another chain,
certification, room rental agreement, pass to some online event, etc.
If the Seller fails to sign on time, the Buyer automatically recovers his stake.

### Problem
  
```js
#lang glow
@interaction([Buyer, Seller])
let payForSignature = (digest : Digest, price : Nat) => {
  deposit! Buyer -> price;

  @Seller let signature = sign(digest);
  publish! Seller -> signature;

  withdraw! Seller <- price;
};
```

### Solution

The above contract publishes the Signature, the Buyer can verify this on their end.
However, since it is not sequenced in the DApp,
while this is happening the Seller can already withdraw their payment.

The solution is to require verification within the DApp before payment can be withdrawn.

```js
#lang glow
@interaction([Buyer, Seller])
let payForSignature = (digest : Digest, price : Nat) => {
  deposit! Buyer -> price;

  @verifiably!(Seller) let signature = sign(digest);
  publish! Seller -> signature;
  verify! signature;

  withdraw! Seller <- price;
};
```

## Contract 2

### Scenario

This contract illustrates how to agree on a mutually random number,
one that both party agree is random, using a three-step commit-reveal protocol.
In this particular contract, the number controls the outcome of a simple bet.
But in general, random numbers can serve as the initial seed for a lot of
randomized algorithms and cryptographic protocols, and it is important that
it is impossible for any party to predict or control it.

### Problem

```js
#lang glow
@interaction([A, B])
let coinFlip = (wagerAmount, escrowAmount) => {
    @A let randA = randomUInt256();
    publish! A -> randA;
    deposit! A -> wagerAmount + escrowAmount;

    @B let randB = randomUInt256();
    publish! B -> randB; deposit! B -> wagerAmount;

    if (((randA ^^^ randB) &&& 1) == 0) {
        //A_wins:
        withdraw! A <- 2*wagerAmount + escrowAmount
    } else {
        //B_wins:
        withdraw! B <- 2*wagerAmount;
        withdraw! A <- escrowAmount
    }
};

```

### Solution

`A` does not hash their random seed. 
As such `B` can view the raw seed,
and abort the transaction if they see their random seed will cause them to lose.

The solution is to hash it, and publish the hash such that the seed is "committed" (i.e. `A` is unable to change it later).

Then later on, only once `B` has deposited their wager amount, we release `A`'s random seed and verify it against the hash.

```js
#lang glow
@interaction([A, B])
let coinFlip = (wagerAmount, escrowAmount) => {
    @A let randA = randomUInt256();
    @verifiably!(A) let commitment = digest(randA);
    publish! A -> commitment; deposit! A -> wagerAmount + escrowAmount;

    @B let randB = randomUInt256();
    publish! B -> randB; deposit! B -> wagerAmount;
    publish! A -> randA;
    verify! commitment;
    if (((randA ^^^ randB) &&& 1) == 0) {
        //A_wins:
        withdraw! A <- 2*wagerAmount + escrowAmount
    } else {
        //B_wins:
        withdraw! B <- 2*wagerAmount;
        withdraw! A <- escrowAmount
    }
};
```

## Contract 3

### Scenario

In this particular contract, two participants play a simple game of rock-paper-scissors.

### Problem

```js
#lang glow

let winner = (handA : Nat, handB : Nat) : Nat => {
    (handA + (4 - handB)) % 3 };

@interaction([A, B])
let rockPaperScissors = (wagerAmount) => {
    @A let handA = input(Nat, "First player, pick your hand: 0 (Rock), 1 (Paper), 2 (Scissors)");
    @A require! (handA < 3);
    @A let salt = randomUInt256();
    @verifiably!(A) let commitment = digest(salt, handA);
    publish! A -> commitment; 

    @B let handB = input(Nat, "Second player, pick your hand: 0 (Rock), 1 (Paper), 2 (Scissors)");
    publish! B -> handB;
    require! (handB < 3);

    deposit! A -> wagerAmount;
    publish! A -> salt, handA;
    require! (handA < 3);
    verify! commitment;
    
    deposit! B -> wagerAmount;

    // outcome: 0 (B_Wins), 1 (Draw), 2 (A_Wins)
    let outcome = winner(handA, handB);

    switch (outcome) {
      // A_Wins
      | 2 => withdraw! A <- 2*wagerAmount
      // B_Wins
      | 0 => withdraw! B <- 2*wagerAmount
      // Draw
      | 1 => withdraw! A <- wagerAmount; withdraw! B <- wagerAmount };

    outcome };
```

### Solution

`A` does not deposit funds until after `B` has published their hand.
This allows them to avoid paying since they know if they will lose.

Even if `A` pays however, `B` can see their hand as well, and decide not to deposit funds.

To fix this,
we want to ensure deposits are made before their hands are revealed.
This prevents the other party from using that information to avoid any losses.

```js
#lang glow

let winner = (handA : Nat, handB : Nat) : Nat => {
    (handA + (4 - handB)) % 3 };

@interaction([A, B])
let rockPaperScissors = (wagerAmount) => {
    @A let handA = input(Nat, "First player, pick your hand: 0 (Rock), 1 (Paper), 2 (Scissors)");
    @A require! (handA < 3);
    @A let salt = randomUInt256();
    @verifiably!(A) let commitment = digest(salt, handA);
    publish! A -> commitment; 
    deposit! A -> wagerAmount;

    @B let handB = input(Nat, "Second player, pick your hand: 0 (Rock), 1 (Paper), 2 (Scissors)");
    publish! B -> handB; 
    deposit! B -> wagerAmount;
    require! (handB < 3);

    publish! A -> salt, handA;
    require! (handA < 3);
    verify! commitment;
    // outcome: 0 (B_Wins), 1 (Draw), 2 (A_Wins)
    let outcome = winner(handA, handB);

    switch (outcome) {
      // A_Wins
      | 2 => withdraw! A <- 2*wagerAmount
      // B_Wins
      | 0 => withdraw! B <- 2*wagerAmount
      // Draw
      | 1 => withdraw! A <- wagerAmount; withdraw! B <- wagerAmount };

    outcome };
```
