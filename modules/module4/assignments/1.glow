#lang glow
// This is a standard closing contract,
// wherein a Buyer pays money in exchange for a Seller signing a transaction:
// digital title transfer, transaction that completes an atomic swap on another chain,
// certification, room rental agreement, pass to some online event, etc.
// If the Seller fails to sign on time, the Buyer automatically recovers his stake.

@interaction([Buyer, Seller])
let payForSignature = (digest : Digest, price : Nat) => {
  deposit! Buyer -> price;

  @Seller let signature = sign(digest);
  publish! Seller -> signature;

  withdraw! Seller <- price;
};
