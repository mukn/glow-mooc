## **Module 6** – Game Theory for DApps

**TEXT / LECTURE 6-1** : Crash course in Mechanism Design

**PRACTICE 6-1** : designing a mechanism diagram / mindmap 

**ASSIGNMENT (later ? ) 6-1** : Establish a diagram / mindmap of a DApp, and analyze diagrams from other developers in the community.
 
**TEXT / LECTURE 6-2** : What are collaterals ? How do we compute them ? How should we price them ?

**PRACTICE 6-2** : ?

**ASSIGNMENT (later ? ) 6-2** : ?
