# ASSIGNMENT 3-1

This assignment will get you to identify logical errors which can / should eventually be automatically caught by the *Glow* compiler.

# Question 1

What is wrong with the following contract?
1. There is no issue.
2. The Buyer did not sign the digest.
3. The Seller should be depositing the price first.
4. The Net Balance of Buyer, Seller and Contract is negative.
5. The Net Balance of Buyer, Seller and Contract is positive (non-zero).

``` js
#lang glow

@interaction([Buyer, Seller])
let payForSignature = (digest : Digest, price : Nat) => {
  deposit! Buyer -> price;

  @publicly!(Seller) let signature = sign(digest);

  withdraw! Seller <- price / 2;
};
```

## Answer

5

## Explanation

The above contract does not end with net zero in the participants' accounts.
There is still half of the Buyer's deposit left in the contract.

## Correct Code

``` glow
#lang glow

@interaction([Buyer, Seller])
let payForSignature = (digest : Digest, price : Nat) => {
  deposit! Buyer -> price;

  @publicly!(Seller) let signature = sign(digest);

  withdraw! Seller <- price;
};
```

# Question 2

What is wrong with the following contract?
1. Nothing is wrong.
2. There are funds stuck in the contract account.
3. There is surplus left in the contract account at the end of the transaction.
4. The Clients did not sign the transaction.
5. Funds were withdrawn to external accounts.

```glow
#lang glow

@interaction([Client1, Client2])
let transferFunds = (price : Nat) => {
  withdraw! Client1 <- price;
  deposit! Client2 -> price;
};
```

## Answer

3

## Explanation

The amount of funds in the contract account will become negative after `Client1` withdraws funds.
Instead, we should first fund the escrow account by `deposit! ...` then after that `withdraw!` the amount.

## Correct Code

```js
#lang glow

@interaction([Client1, Client2])
let transferFunds = (price : Nat) => {
  deposit! Client2 -> price;
  withdraw! Client1 <- price;
};
```

# Question 3

What is wrong with the following contract?
1. There are uses of random variables, which makes the outcome uncertain.
2. There is deficit made in contract account due to excessive withdrawals.
3. B can never win.
4. A can never win.
5. Funds are withdrawn to external accounts.

```js
#lang glow

@interaction([A, B])
let coinFlip = (wagerAmount, escrowAmount) => {
    @A assert! canReach(A_wins);
    @A let randA = randomUInt256();
    @verifiably!(A) let commitment = digest(randA);
    publish! A -> commitment; deposit! A -> wagerAmount + escrowAmount;

    @B assert! canReach(B_wins);
    @B let randB = randomUInt256();
    publish! B -> randB; deposit! B -> wagerAmount;
    publish! A -> randA;
    verify! commitment;

    B_wins:
    withdraw! B <- 2*wagerAmount;
    withdraw! A <- escrowAmount
};
```

## Answer

4

## Explanation

We observe that there are two assertions made: `A_wins`, `B_wins`.
However, we can never reach `A_wins`, because there isn't a label for it! Only one exists for `B_wins`.

## Correct Code

```js
#lang glow

@interaction([A, B])
let coinFlip = (wagerAmount, escrowAmount) => {
    @A assert! canReach(A_wins);
    @A let randA = randomUInt256();
    @verifiably!(A) let commitment = digest(randA);
    publish! A -> commitment; deposit! A -> wagerAmount + escrowAmount;

    @B assert! canReach(B_wins);
    @B let randB = randomUInt256();
    publish! B -> randB; deposit! B -> wagerAmount;
    publish! A -> randA;
    verify! commitment;
    if (((randA ^^^ randB) &&& 1) == 0) {
        A_wins:
        withdraw! A <- 2*wagerAmount + escrowAmount
    } else {
        B_wins:
        withdraw! B <- 2*wagerAmount;
        withdraw! A <- escrowAmount
    }
};
```

# Question 4

## Question

What is wrong with the following contract?
1. Nothing is wrong
2. Only A can win
3. Only B can win
4. Invalid inputs are allowed
5. Funds are withdrawn to External accounts

```js
#lang glow

let winner = (handA : Nat, handB : Nat) : Nat => {
    (handA + (4 - handB)) % 3 };

@interaction([A, B])
let rockPaperScissors = (wagerAmount) => {
    @A let handA = input(Nat, "First player, pick your hand: 0 (Rock), 1 (Paper), 2 (Scissors)");
    @A require! (handA < 3);
    @A let salt = randomUInt256();
    @verifiably!(a) let commitment = digest(salt, handA);
    publish! a -> commitment;
    deposit! a -> wagerAmount;

    @B let handB = input(Nat, "Second player, pick your hand: 0 (Rock), 1 (Paper), 2 (Scissors)");
    publish! b -> handB;
    deposit! b -> wagerAmount;
    require! (handB < 3);

    publish! a -> salt, handA;
    require! (handA < 3);
    verify! commitment;
    // outcome: 0 (B_Wins), 1 (Draw), 2 (A_Wins)
    let outcome = winner(handA, handB);

    switch (outcome) {
      // A_Wins
      | 2 => withdraw! a <- 2*wagerAmount
      // B_Wins
      | 0 => withdraw! b <- 2*wagerAmount
      // Draw
      | 1 => withdraw! a <- wagerAmount;
      withdraw! b <- wagerAmount };

    outcome };
```

## Answer

5

## Explanation

There was a typo in the contract, where all participants' symbols were lowercased.
As such the funds in the contracts are being withdrawn to non-existent account holders.

## Correct Code
``` sh
#lang glow

let winner = (handA : Nat, handB : Nat) : Nat => {
    (handA + (4 - handB)) % 3 };

@interaction([A, B])
let rockPaperScissors = (wagerAmount) => {
    @A let handA = input(Nat, "First player, pick your hand: 0 (Rock), 1 (Paper), 2 (Scissors)");
    @A require! (handA < 3);
    @A let salt = randomUInt256();
    @verifiably!(A) let commitment = digest(salt, handA);
    publish! A -> commitment; deposit! A -> wagerAmount;

    @B let handB = input(Nat, "Second player, pick your hand: 0 (Rock), 1 (Paper), 2 (Scissors)");
    publish! B -> handB; deposit! B -> wagerAmount;
    require! (handB < 3);

    publish! A -> salt, handA;
    require! (handA < 3);
    verify! commitment;
    // outcome: 0 (B_Wins), 1 (Draw), 2 (A_Wins)
    let outcome = winner(handA, handB);

    switch (outcome) {
      // A_Wins
      | 2 => withdraw! A <- 2*wagerAmount
      // B_Wins
      | 0 => withdraw! B <- 2*wagerAmount
      // Draw
      | 1 => withdraw! A <- wagerAmount; withdraw! B <- wagerAmount };

    outcome };
```
