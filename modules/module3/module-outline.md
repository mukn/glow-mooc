##APPENDIX 3-1 : THE COMMON THREATS (definition and examples)

* Authentication
* Privacy
* No cooperation
* Replay attacks
* DDoS
* Economic DoS
* race conditions
* front-running
* “free option” for whoever’s turn it is
* capital immobilization
* interests paid or forfeit
* authentication failure
* revealing information (too early)
* forks and replays
* (non)repudiation
* man-in-the-middle
* supply-chain attacks
* underhanded code to violate authentication or privacy or application logic
* cryptological attacks
* meta-data privacy
* etc.
