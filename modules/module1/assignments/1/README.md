# Module 1 Assignment

As mentioned in **VIDEO 1-3** and  **TEXT 1-3**, to verify everything works, you can run our [test suite](https://gitlab.com/mukn/glow/-/blob/master/INSTALL.md#testing-glow).

You should be in the project root, where `unit-tests.ss` is located:

``` sh
> echo $PWD
/../glow

> ls
unit-tests.ss <other-files>
```

The commands to run are:

``` sh
> ./unit-tests.ss # For Unit Tests
> ./unit-tests.ss integration # For Integration Tests
```

## Expected results from running tests

### Unit Tests

All Tests should reflect "OK".

``` sh
> ./unit-tests.ss
...
Test case: supply-parameters: buy_sig
... 0 checks OK
Test case: initial block
... 0 checks OK
... All tests OK
--- Test Summary
test suite for glow/compiler/project: OK
...
test suite for glow/t/cli-integration: OK
OK
```

1. Input the full test summary section to verify your answers, i.e.:
```
--- Test Summary
test suite for glow/compiler/project: OK
<replace this with rest of the test output>
OK
```

### Integration Tests

All Tests should reflect "OK".

``` sh
> ./unit-tests.ss integration
...
t/Bob (0xb0bb1ed229f5Ed588495AC9739eD1555f5c3aabD) has 4.99999999999985254 ETH already. Good.
t/Trent (0x73e27C9B8BF6F00A38cD654079413aA3eDBC771A) has 2 ETH already. Good.
DONE
Test case: rps_simple executes
outcome extracted from contract logs, 0 (B_Wins), 1 (Draw), 2 (A_Wins):2... check outcome is equal? to 2
... 1 checks OK
... All tests OK
--- Test Summary
integration test for ethereum/buy-sig: OK
integration test for ethereum/coin-flip: OK
integration test checking escrow return on timeout: OK
integration test for ethereum/rps_simple: OK
OK
```

2. Input the full test summary section to verify your answers, i.e.:
```
--- Test Summary
integration test for ethereum/buy-sig: OK
<replace this with rest of the test output>
OK
```
