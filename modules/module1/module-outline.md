## **Module 1** : Installing Glow

**VIDEO 1-1a and TEXT 1-1a** : step by step explanation to set up a docker image and connect it either to the EVM or the Cardano Testnet on MAC OS

**VIDEO 1-1b and TEXT 1-1b** : step by step explanation to set up a docker image and connect it either to the EVM or the Cardano Testnet on a Linux distribution.

**VIDEO 1-2a and TEXT 1-2b** : step by step explanation to install Glow from the command line via NixOS, on both MAC OS or a Linux distribution

**VIDEO 1-3 and TEXT 1-3** : how to check that everything works (funds, wallet, etc…)
FAQ 1-1 : Common installation problems and how to solve them

**ASSIGNMENT 1-1** : ?
