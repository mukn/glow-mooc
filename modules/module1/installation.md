# Installing Glow

## Introduction

Hi there, I'm Alex Plotnick, and this is Module 1
of the first-ever Glow online course. In Module 0,
Faré and I gave a brief overview of Glow and the
course as a whole.

In this Module, we'll be installing and setting up the
Glow system so that we're ready to start learning the
language in Module 2.

## Essential docs & pages

So, the best place to start with Glow is https://glow-lang.org
There you'll find tutorials, reference materials, etc.

Another useful place to look for information is the Glow
source code repository at https://github.com/Glow-Lang/
and for this lecture in particular,
https://github.com/Glow-Lang/glow/blob/master/INSTALL.md
has the installation instructions.

[Also important: issues.]

## Overview and requirements

So, there are at least three ways of installing Glow:

1. You can run our one-liner that uses Nix under the hood,
2. you can use our pre-built docker image, or
3. you can clone the repository and build it from scratch.

I'm going to focus mainly on the last, since it's the
simplest and easiest to work with, but I'll touch on
the Docker installation at the end.

The way our line-liner works is it installs the Nix
package manager, and then uses that to install pre-built
(cached) Glow packages for Nix. So I'm going say a few
words about Nix and why we use it, but in your day-to-day
use of Glow it shouldn't really come up very often.

The main reason we use Nix is that it provides *reproducible*,
*verifiable* packages. What that means is that if I build the
Glow package on my machine, and you build it on your machine,
the packages we end up with will be bit-for-bit identical. And
if either of us were to change any bit of any source file, they
would no longer be. If you've ever dealt with packaging, that may
sound like black magic and like it couldn't possibly be true, but
it is. And that's why we use it: we believe that everyone that
wants to should be able to audit Glow's source code, build it by
hand, build packages and verify that they exactly match ours, and
build their own packages customized to their needs. Nix helps us
ensure that we're all talking about exactly the same version of
Glow built using the exact same dependencies.

But that's all just background. The reason I'm mentioning it is
that you'll see some Nix stuff as I go along, and I wanted it
to not be some mysterious black box.

Ok, let's get concrete. Here's our one-liner:
`curl -L https://glow-lang.org/install/glow-install | sh`

It uses `curl` to fetch a shell script, then pipes it to
the shell for evaluation. If you don't have `curl`, you could
install it with something like:
`apt-get install curl`.

I'm going to run this on Debian Linux, but it should work
exactly the same on any other Linux distribution, or macOS.
We're curious if it works under Windows with WSL2; if you
try that, please let us know how it goes!

## Installing glow

All right, let's try it out.

`curl -L https://glow-lang.org/install/glow-install | sh`

As you can see, this downloads a compressed tarball with
the Nix installer. So while that's going I'll mention one
or two other requirements that you *might* not have on your
system, though you probably do. As you see here, this tarball
is compressed using the `xz` compression scheme, and if you're
working in a very bare system (like a VM or docker image),
you might need to run something like `apt-get install xz-utils`.
Another requirement is `sudo`, which lets you run programs
(namely, the Nix installer) as root without having the root
password. `sudo` is fairly ubiquitous, but again if you're
on a minimal machine you might need to install it. You might
also find that it asks you for a password during the installation
process; that's perfectly normal, and you should type your own
password, but you won't see the characters echo on the terminal.

Ok, now you can see Nix doing some package installations.
By default, Nix puts everything in a directory called `/nix`.
The current Glow packages and all of its dependencies (and
nothing else) currently requires about 1.5 GB of space.

Now, some systems may not have a few free GB in their root
partitions. If you're on Linux, you can get around this with a
*bind mount*, which lets you mount some part of your filesystem
in another place. For instance, on my system, I have a big `/var`
partition but a small root; when I needed space for Nix, I added
an entry to my `fstab` that remaps part of the `/var` partition
under `/nix`, so there's plenty of room.

(You may think that you could just use a symbolic link,
and that's a good thought, but it doesn't work for reasons
you can Google. So a bind-mount is currently the best strategy
if you have a small root partition.)

Ok, and it's finally done! You can see here it's installed
a few dozen dependencies, mostly related to Gerbil Scheme,
which is what Glow is written in. And here's `go-ethereum`,
or `geth`, which we'll use as a test network in just a few
minutes. And finally Glow itself. I'll let you read this text
at the end yourself, but basically it's just saying that Glow
is installed, but you'll have to adjust your path to find it,
either manually or by launching a new terminal. So let's close
this one and open a new one up...

## Running glow

Ok, now I can run `which glow`, and it shows me a local
link to the `glow` program. This is the program we're
going to use for essentially the whole course, so let's
spend a minute getting acquainted with it.

The `glow` program is like many newer command-line programs,
in that it does a whole bunch of related things. Each of those
sub-commands has a name, which we can see if we run `glow`
with no arguments, or with the `help` command. For instance,
`glow version` prints the version number and exits. A slightly
more interesting command is `glow list-applications`, which
shows which DApps Glow knows about and where they are. These
are simple programs written in the Glow language, which we'll
start learning in Module 2. So for now let's stick to the
commands that don't involve the langauge.

One last important note on this: `glow help` can also
take a command name, like `glow help list-applications`.
This shows the command-line arguments specific to that
command. This one only takes one argument, but a lot of
them take many, and so it's really handy to be able to
look them up easily.

### Identities

One useful set of commands lets you manipulate addresses
and identities. Let's start by asking Glow who it knows
about: `glow list-identities`. Unsurprisingly, right now
it doesn't know about anyone.

Let's make a new address on a chain. The first question is
of course *which* chain? Right now, Glow supports several
EVM-based chains, which we can see with `glow list-evm-networks`.
(I'll use the terms "network" and "chain" mostly interchangeably,
though maybe I shouldn't.)

For this course, we'll stick mostly with just two networks:
`ced` the Cardano EVM Devnet (which is Cardano + EVM compatibility),
and `pet`, which is a private testnet that we can run with
`geth`. Let's see some examples.

So, the command to generate a new identity in Glow is
called `glow generate-identity`. Let's look at the help
for that command:
`glow help generate-identity`

We'll need to use the `--evm-network` and `--nickname`
options, to tell Glow what network our new identity is
for and what we'd like to call it. The nickname is just
a label we'll use to refer to this otherwise-anonymous
address.

`glow generate-identity --evm-network ced --nickname c`
`glow list-identities`

All right, now we have an identity, which is to say
an address on a particular blockchain, as we can see
in the output of `glow list-identities`. This little
key icon means that Glow controls the private key for
this address, which is synonymous with identity on the
blockchain. And here's our nickname `c` in parenthesis.
We can generate as many of these as we like:

`glow generate-identity --evm-network ced --nickname d`

And we can remove identities easily, too:

`glow remove-identity --nickname d`
`glow list-identities`

Ok, now you're probably wondering about these weird `<anonymous>`
strings and these mysterious numbers. These are *contacts*, and
here we mean that like in "Google Contacts". A contact can have
multiple identities, just like a contact on your phone can have
multiple phone numbers, email addresses, etc. Each contact has
a (non-globally-unique) ID number, which is what you see here.
Contacts can also have names, but because the identities we
generated earlier were anonymous, these don't.

So let's remove this empty one:
`glow remove-contact --cid 2`
`glow list-contacts`

And now let's add a new one, and give it a name.
"Alice" is the standard name, but my name is "Alex",
so I'll use that.

So we can add a contact with
`glow add-contact --name Alex`
`glow list-contacts`

And now we can add an identity, i.e., an address,
associated with that contact:
`glow generate-identity --cid 2 --nickname a --evm-network ced`

So the reason I have to give the network here, but not
when I created the contact, is that a contact may have
known addresses on several networks.

For instance, I can make another identity for myself
on the local test network:
`glow generate-identity --cid 2 --nickname b --evm-network pet`
`glow list-identities`

Ok, now you don't need to just generate identities.
You can also import them; if Bob emails you his address,
you can import it into Glow using one of these commands.
So, here's a bit of homework: find some address on an EVM
network (it could be yours, or some public address, it
doesn't matter), and add it as an identity under a contact
in Glow. For bonus points, include the corresponding public
key.

### Funding & transfers

All right, let's finish up with a little demo of actually
interacting with the chain. So we have these identities,
but they don't have any tokens. Let's get them some!

On a real network, you usually have to pay for tokens.
But on a test network, there's usually some "faucet"
address that can serve as a source of tokens for testing.

So let's use that:
`glow faucet --to a --evm-network ced`
And... ("Waiting for confirmation...") *this*
is why I recommend using the local test network.
Ok, finally finished; we have 1 CED in our account!
Hooray, at last we can retire and give up this life
of hacks!

Well, maybe not. All right, let's fund another one.
`glow faucet --to b --evm-network pet`
Ahhhh, what happend? Well, it can't connect to the
local server. Why not? Oh, because we forgot to start it!
So let's do that.

We provide a script that runs `geth` in just the right way
for Glow and some of its tests. The script is called
`run-ethereum-test-net`. And you can see here it's starting
a server at that address it failed to connect to a minute ago.

So we'll try again:
`glow faucet --to b --evm-network pet`
Aha! Success. We now have 5 units of a valueless test
token. At last we can retire!

All right, maybe not. Let's do one more trick with these
tokens, and then we'll call it a day. I'm going to make
one more identity on the local test network:
`glow generate-identity --cid 2 --nickname d --evm-network pet`
`glow list-identities`

And now instead of funding `d` from the faucet, let's
share some of `b`'s tokens:
`glow transfer --from b --to d --value 2 --evm-network pet`
And now you see that `b` has 3 PET minus a small gas fee,
and that `d` has exactly 2 PET.

And again, if you're trying to run commands like these but
can't remember all the arguments, remember that you can always
run `glow help <whatever>`, e.g., `glow help transfer`.

## Other installation procedures

So, as I said at the beginning, we've covered one of the three
standard installation procedures. The other two are building
from source, which I won't get now, and using our Docker image,
which is also very easy:

`docker pull mukn/glow:latest`
`docker run -it mukn/glow:latest`
`glow version`

To make this useful if you're writing your own Glow DApps,
you'll probably want to mount a directory for them, and
also probably one for your Glow config and database directories;
there are examples of this in the `glow/INSTALL.md` file.
I personally find working with compilers inside Docker images
kind of unwieldy, but the advantage is that it doesn't need to
install anything on your local system.

You can also write make own Docker image pretty easily
using the `glow-install` script; we'll post an example
for you if you want to try that out. You can also of course
use any other VM or container you happen to like, and may
need to do so if you're on Windows or another unsupported
operating system.

## Wrapping up

All right, so to recap: you can install glow with one command:
`curl -L https://glow-lang.org/install/glow-install | sh`
This installs Glow via its Nix package, which puts everything
in `/nix`. Once the installation's complete, you can run
`glow help` and other Glow commands. You can also use our
pre-built Docker image.

We've also seen how to do basic contact and identity management
with Glow, and how to do simple interactions between identities.
All of that is just for us to keep things straight as humans;
Glow really only cares about the address, a private key if it
has one, and which network it's running on.

And of course, please let us know if you encounter any problems
or have any questions during your installation of Glow. Please
post them to #glow-mooc on Discord, or DM me at @afp.

All right, thanks very much for watching, and we hope you
have a smooth Glow installation! We'll see you live for
Module 2, where we'll dig into the language and actually
running DApps.

Bye!
