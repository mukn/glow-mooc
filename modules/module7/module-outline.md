## **Module 7** – User interface for Glow DApps

**TEXT 7-1** : Basic concepts in Javascript

**PRACTICE 7-1** : Customizing the auto-generated interface in JavaScript

**PRACTICE 7-2** : Creating new components in JavaScript
