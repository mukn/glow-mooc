## **Module 0** : Onboarding

**VIDEO 0-1** : 3 co-founders of Mutual Knowledge Systems explaining the goal of the course

**TEXT 0-1** : Similar to edX : presentation of the teachers, presentation of the course, presentation of the ideal prerequisites, and possible resources if the student hasn’t got a strong enough background

**VIDEO 0-2** : Presentation of your teachers

**TEXT 0-2** : Key information about Glow (DSL, adversarial environment, when it is useful, difference between DApp and Smart Contract and so on)

**TEXT 0-3** : Crash introduction to decentralized applications and smart contracts

**ASSIGNMENT 0-1** : Multiple choice questions about text 0-2

**APPENDIX 0-1** : Bibliography and external resources (our Gitlab, basic programming tutorials if needed, our channels, our reference manual, etc…)
