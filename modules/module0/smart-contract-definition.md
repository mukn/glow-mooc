A smart contract is a program stored on a blockchain that runs when a set of predefined conditions are met. 

A smart contract automates the execution of an agreement to achieve consensus among parties without the need for an intermediary, such as a notary or a lawyer. 

DApps rely on smart contracts for logic processing. 

A smart contract therefore controls some of the events triggered as part of a decentralized application, rather than a single company, or other type of entity. 
