# Introduction to the Glow MOOC

## What is Glow, and what for?
[Faré]

[Slide 1]

Hello there,

my name is François-René Rideau.
Im am CEO of [MuKn](https://mukn.io), and
lead architect of [the *Glow* language](https://glow-lang.org).
But you can call me *Faré* -- that's my name on [Discord](https://discord.gg/pPMcqD6hWz).

In this MOOC, or Massive Open Online Course,
our CTO Alex Plotnick will teach you how to use *Glow*:
our Domain Specific Language for Decentralized Applications.

But just what is a Domain Specific Language?
And first, what is a Decentralized Application?
That's what I would like to briefly explain
before we start getting deeper.

### Decentralized Applications

[Slide 2 - Faré]

You probably already have some idea of what a Decentralized Application is,
since you came to this course.
But let me try to define the concept clearly.

A Decentralized Application, or DApp, is:
an *interaction*,
between multiple, *untrusting*, parties,
exchanging *assets*, on public ledgers,
subject to *algorithmically verifiable* rules.

These are four criteria for makes a DApp.

#### Interaction

[Slide 3 - Faré - zoom on word "interaction" from slide 2, show kinds of interactions, zoom back to slide 2]

The interaction may involve trading, insurance, future options, following market price charts,
supply chain events, recording real-estate deals or medical diagnoses and interventions,
maybe even gambling. It can be any kind of commercial transaction, of professional or hobby activity.

#### Untrusting

[Slide 4 - Faré - zoom on word "untrusting" from slide 2, show reasons for distrust, zoom back to slide 2]

The parties may be unable to otherwise trust each other because they
live in *far away countries*.
Maybe one lives in Kenya and the other one in Kazakhstan.
If one reneges on his obligations, the other will have no recourse.

Or maybe their *interaction is too small* to justify involving lawyers.
*In case of a dispute*,
maybe neither of them has the *means to afford a lawyer* or to be worth targeting by a lawyer.

In any case, the "official" enforcement mechanisms don't apply or are too expensive.
But a DApp can help.

#### Assets

[Slide 5 - Faré - zoom on word "assets" from slide 2, show reasons for distrust, zoom back to slide 2]

The assets at stake in a DApp may be some tokens natively managed by a blockchain,
like Bitcoin, Ethereum or Cardano.

But they may be any kind of goods or services somehow tracked on a blockchain:
paintings, real estate, cars or rooms sold or rented, haircuts or plumbing interventions.
Anything goes, as long as some “oracle”, centralized or decentralized,
can be trusted by all parties to correctly reflect the ownership of these real world assets
on a blockchain.

#### Algorithmically Verifiable

[Slide 6 - Faré - zoom on words "algorithmically verifiable" from slide 2, show points (a) whether a party behaved or misbehaved, (b) a computer program can tell, (c) and take enforcement measures, (d) both parties can understand and agree. Zoom back to slide 2]

The rules of the interaction must be algorithmically verifiable,
which means that a computer program can automatically tell
whether one party followed the rules or misbehaved, and make the victim whole by punishing the culprit.
This program that serves as automated arbiter is called a *smart contract*,
and runs on a public *blockchain*.
Both parties must trust the program to enforce the proper rules,
possibly by trusting experts who wrote or audited the program and vouched for it.

### Domain Specific Language

[Slide 7 - Faré - ]

Now a Domain Specific Language, or DSL, is a *programming language*
designed to precisely express and combine the *concepts* of a given domain.
Compared to other languages, a DSL enables you to write programs that are
*simpler*, *safer* and more *portable*.

#### Domain concepts vs Implementation details

A DSL is a *high-level* language, which is
as much about what you *can* talk about
as it is about what you *don't have to* think about.

On the one hand, in *Glow*, you *can* talk about
the *domain concepts* that matter to decentralized applications:
participants, assets, amounts, durations, etc.

On the other hand, in *Glow*, you *don't have to* think about
*implementation details* that are not interesting from the commercial point of view,
yet that lower-level languages force you to deal with:
sending and receiving network messages made of bits and bytes;
managing keys so there is no mixup who signs and sends what messages to whom;
inverting control flow into data flow so you can relate sequences of messages in a same interaction;
handling timeouts so funds are not lost when one participant stops cooperating;
synchronizing the behavior of off-chain clients and on-chain;
escrowing sufficient collateral to keep every participant honest;
etc.

#### Simplicity

Not having to think about all these gory details
means that programs can be simpler, easier, faster and cheaper
to write, to understand, to audit, to trust.

It also frees your mind and your imagination,
so you can build more elaborate applications;
applications that would not have been possible to build at all
if you had to deal with all the details.

#### Safety

"Smart contracts" written in Solidity have lost hundreds of millions of dollars worth in assets,
due to reentrancy bugs, mis-authentication bugs, unbalanced accounting bugs,
library destruction bugs, etc.

These are bugs that are not even possible to express in *Glow*:
they involve low-level implementation details in Solidity
for which a programmer failed to consider some case, sometimes subtle, sometimes not so.
But *Glow* abstracts these details away, and automatically covers
all the cases for you, subtle or not.

Not having to get the details right means much fewer opportunities to get them wrong.
*Glow* programs are not only an order of magnitude simpler
than their equivalent in Solidity plus JavaScript;
they are an order of magnitude safer, for the same reason.

#### Portability

A DSL abstracts away implementation details
so you can focus on the essential concepts of its domain.
Then, the language can be adapted to run in very different environments.
Thus the *Glow* compiler can be adapted to support any blockchain
and a program written in *Glow* can run on
any blockchain supported by the *Glow* compiler.

By contrast most or all "smart contract" languages only run
on a single blockchain, and its clones if any.
If you write a "smart contract" in Solidity,
it will only run on Ethereum and its variants.
Similarly, Plutus will let you write code only for Cardano,
Cashscript only for Bitcoin Cash, etc.
Whatever you use will work only on one type of blockchain;
and then you'll need also to write your off-chain client programs,
also in a different language, with a different API.

### Overview Conclusion

### Summary

Using *Glow*, you can concentrate on just *writing your program*.
It's just like using Assembly instead of binary code, C instead of Assembly,
Python instead of C, or Lisp instead of Python:
each higher-level language abstracts away some low-level details from previous languages,
that are efficiently handled by its compiler, so you don't have to deal with them.

Thus your decentralized applications can be *simple*, *safe* and *portable*.
They will be cheaper to build and easier to trust,
because you can focus on the essential concepts that matter,
while *Glow* handles implementation details for you.

### Already useful, but a Work in Progress

As of today, the *Glow* compiler only supports Ethereum and Cardano and their variants.
But we are actively working on supporting more.
If there's a blockchain that *you* would like *Glow* to support next, let us know!

The *Glow* language can already be used to write many useful decentralized applications.
Yet we have many plans for how to improve it, and
we are working hard to extend it so that *Glow* will support
all the Decentralized Applications that matter.
Is there a specific DApp that you would like to write, for which today's *Glow* is insufficient?
Again, let us know.

We invite you to fill our questionnaire, linked below,
and tell us what is important to you, so we can better prioritize
which features to implement next.

Now I'm going to hand things over to our CTO Alex Plotnick,
who's going to lead the lectures.
I'll be around to answer questions and help out, though, so I'll see you all soon!

## Welcome to the *Glow* MOOC

[Alex]

Thanks, Faré. Hi, everyone, and welcome to the very first
Glow MOOC: our online course on the *Glow* language for DApps.

I'm Alex Plotnick, one of the three Alexes at MuKn.
I'm `@afp` on [Discord](https://discord.gg/pPMcqD6hWz).
I'm also the newest Alex, so am definitely going to depend on
Faré and the rest of the development team to answer the really tough questions.

### Goals and Prerequisites

#### Prerequisites of this MOOC

Faré talked about some of our assumptions behind the
design of Glow, and this course. Here's what we'll
assume about you in this course.

We assume that you have some background in programming —
*Glow* is primarily a programming language, and
not (currently) aimed at non-technical users.
We don't assume you have a lot of experience,
but we assume you know what a programming language is,
roughly what a compiler does, and what functions,
variables, and constants are.

We assume you are interested in building your own DApps.
You definitely don't need to have experience writing DApps,
or even a concrete idea for what DApp to write (though that would help!).

We also assume that you understand the very very basics of
blockchains: what a token is, an address, a transaction. We assume
you have at least heard of smart contracts and a rough idea of how
they fit in that universe. We'll also assume a bit of familiarity
with public-key cryptography, though we'll certainly explain details
as we need them.

If you don't know anything about all of those things, don't panic!
We'll cover a lot of ground in this course, and if you don't follow all of it,
we hope you'll still get something out of the rest.
We've also got lots of references in our bibliography,
and are *always* happy to answer questions.

#### Goals of this MOOC

In this course, you're going to learn the syntax and semantics of *Glow*.
That is, you'll be able to read, and write,
simple interactions written in the *Glow* language,
and more importantly understand *why* they're written the way that they are.

We hope that after taking this course, you'll go out and write all
your Decentralized Applications in *Glow*. And we hope that any time
you want to write a "smart contract", you'll think about writing it
with *Glow*. But even if you never use *Glow* again, we hope that
you'll learn in this course ways to build safer, *smarter* contracts
no matter how you write them.

In addition to learning the nuts and bolts of *Glow*, we're also going to cover:

- Writing DApps and what can go wrong with that.
- DApp models and threat models.
- A bit of fancy Logic and a little Game Theory.
- Some User Interface topics.
- And a bit about on-chain and off-chain communication.

### Plan of the Course

All right.
Let's call this introduction that you're watching right now "Module 0" of this course.
"Module 1" will have been released at the same time, and that's about
downloading, installing, and setting up *Glow*.
You'll need to have a working *Glow* installation to follow along with Module 2,
which will be our first live class, and cover the basics of the language and run-time.

So the next step is to go watch Module 1, and if you have any questions,
if you get stuck, or have suggestions, please let us know on [Discord](https://discord.gg/pPMcqD6hWz).
Again, I'm `@afp`, and feel free to DM me, or ask for help on `#glow-mooc`.

One last note on the expected time requirements for this course.
We'll be holding live classes and posting the videos once every
two weeks. And the videos will probably be around 60-90 minutes.
We will occasionally assign homework, and like any course, the
more you put into that on your own time (or with others, which
I particularly encourage - I love study groups), the more you'll
get out of it. But we understand that not everyone has a lot of
excess time, and so if you'd like to just "audit" the course and
follow along with the videos, that'll probably be just fine, too.

That's it for now. Thanks very much for joining us, and happy DApp
hacking!
