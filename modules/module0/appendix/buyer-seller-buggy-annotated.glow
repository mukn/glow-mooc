#lang glow

// Example taken from François-René Rideau's (@fare) talk: https://glow-lang.org/talks/security-through-clarity-2021/

// We define an interaction between 2 participants, who are transacting an asset called price.
@interaction({participants=[Buyer, Seller], assets=[price]})

// The interaction is called closing,
// and it takes as parameter the cryptographic digest of a document,
// that the Seller will sign electronically.
let closing = (digest : Digest) => {
  deposit! Buyer -> price; // First, the Buyer deposits the agreed-upon price in escrow.

  // Now the Seller becomes active.
  // The Seller signs the digest in private, publishes signature which everyone can verify.
  @publicly!(Seller) let signature = sign(digest);
  withdraw! Buyer <- price; // Finally the Buyer takes the money out of the contract
                             // This is incorrect however!
                             // The Seller should have received the payment instead.
  return signature; // Return the signature for further use
};
