# Buyer Seller Interaction

Extracted from *François-René Rideau's (@fare)* talk: https://glow-lang.org/talks/security-through-clarity-2021/

## Scenario

Let's consider the simplest possible Decentralized Application (DApp): the closing of a sale. The intent of the DApp is as follows: a Buyer and a Seller have agreed to the terms of a sale. The Seller will sign a title transfer, and the Buyer will pay a price for it in tokens. The title transfer may be an electronic copy of a legal document, a transaction on another blockchain, a keycode for a hotel room, or anything in between, etc.

Because they want the transaction to be trustless, the two participants use a blockchain smart contract to ensure that their interaction will be “atomic”, i.e. all or nothing: either they both cooperate and the transaction happens, or one fails to cooperate and then nothing bad happens to the other participant except wasting a bit of time and a small amount of transaction fees.

The sequence diagram for a successful interaction will be as follows:

![](../../../Diagrams/closing.png)

How can we implement this DApp with standard development tools, and what kind of errors must we guard against?

## Implementations

[`buyer-seller-buggy.sol`](buyer-seller-buggy.sol) demonstrates a buggy (unannotated) solution in `Solidity` for you to hunt the bug down.

[`buyer-seller-buggy-annotated.sol`](buyer-seller-buggy-annotated.sol) demonstrates a buggy (annotated) solution in `Solidity` which describes the issues with the contract.

[`buyer-seller-buggy.glow`](buyer-seller-buggy.glow) demonstrates a buggy (unannotated) solution in `Glow` for you to hunt the bug down.

[`buyer-seller-buggy-annotated.glow`](buyer-seller-buggy-annotated.glow) demonstrates a buggy (annotated) solution in `Glow` which describes the issues with the contract.

[`buyer-seller-fixed.glow`](buyer-seller-fixed.glow) demonstrates a working solution in `Glow`.
