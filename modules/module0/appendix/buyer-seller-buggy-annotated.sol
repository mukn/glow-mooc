pragma solidity ^0.8.2; // SPDX-License-Identifier: Apache2.0

// Example taken from François-René Rideau's (@fare) talk: https://glow-lang.org/talks/security-through-clarity-2021/
contract ClosingBug {
  // Declaring contract fields
  address payable Buyer;
  address payable Seller;
  bytes32 digest;
  uint price;

  // Initialising contract fields
  constructor(address payable _Buyer, address payable _Seller,
              bytes32 _digest, uint _price) payable {
    Buyer = _Buyer;
    Seller = _Seller;
    digest = _digest;
    price = _price;
    require(msg.value == price); // The initialization requires a deposit made in the transaction
  }

  // Declare an event which tells us when the Seller has published their Signature
  event SignaturePublished(uint8 v, bytes32 r, bytes32 s);

  // The sign function is called by the seller
  function sign(uint8 v, bytes32 r, bytes32 s) public payable {
    require(Seller == ecrecover(digest, v, r, s)); // Obtain seller address
    emit SignaturePublished(v, r, s); // Indicate Seller signature has been published
    selfdestruct(payable(msg.sender)); // Release the money the msg.sender
  }
}

// There are 2 bugs in the above code.
// 1. If the Seller never signs, the buyer's funds will be stuck in escrow
//    The Buyer never gets anything in return as a result.
//
// 2. The contract releases money to `msg.sender` rather than Seller.
//    This means they can watch for Seller's signing message,
//    change `msg.sender` on it to themselves,
//    repost the signature with more GAS / mine it,
//    front-running the Seller's transaction and getting the money.
