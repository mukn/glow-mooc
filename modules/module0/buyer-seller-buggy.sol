pragma solidity ^0.8.2; // SPDX-License-Identifier: Apache2.0

// Example taken from François-René Rideau's (@fare) talk: https://glow-lang.org/talks/security-through-clarity-2021/
contract ClosingBug {
  address payable Buyer;
  address payable Seller;
  bytes32 digest;
  uint price;

  constructor(address payable _Buyer, address payable _Seller,
              bytes32 _digest, uint _price) payable {
    Buyer = _Buyer;
    Seller = _Seller;
    digest = _digest;
    price = _price;
    require(msg.value == price);
  }

  event SignaturePublished(uint8 v, bytes32 r, bytes32 s);

  function sign(uint8 v, bytes32 r, bytes32 s) public payable {
    require(Seller == ecrecover(digest, v, r, s));
    emit SignaturePublished(v, r, s);
    selfdestruct(payable(msg.sender));
  }
}
