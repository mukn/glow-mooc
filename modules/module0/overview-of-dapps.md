A decentralized application (dApp) is a program of application that exists and runs on a blockchain, or a peer-to-peer (P2P) network. 

Cryptocurrencies are built on blockchains, which enables users from around the world to contribute and benefit, since there is no single point of ownership and control. 

There are also security benefits resulting from not having a single point to target and attack, since communications and data storage are encrypted and distributed across all nodes in the network. 
