src = index.rkt
lib = utils.rkt

all: public

.PHONY: all public mrproper clean doc public/docs

public: $(wildcard index.md modules/* modules/*/* modules/*/*/* modules/*/*/*/* Diagrams/*/*)
	mkdir -p public
	rsync -a index.md modules Diagrams public/
	for i in $$(find public -name '*.md') ; do j=$${i%.md} ; markdown < $$i > $$j.html ; done
	for i in $$(find public -name '*.plantuml') ; do plantuml $$i ; done

clean:
	rm -r public

mrproper:
	git clean -xfd
