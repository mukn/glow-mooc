# Welcome to the Glow MOOC

The Glow MOOC is a Massive Open Online Course to teach you how to use
[*Glow*](https://glow-lang.org), the radically simple, portable language for Decentralized Applications.

[**Register NOW for the Glow MOOC starting this September 28th 2021!**](https://j.mp/GlowMOOC2021)
